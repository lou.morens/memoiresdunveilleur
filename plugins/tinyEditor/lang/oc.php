<?php
$LANG = array(
	'L_TINYEDITOR_words'			=> 'mots',
	'L_TINYEDITOR_cancel'			=> 'Anullar',
	'L_TINYEDITOR_submit'			=> 'Mandar',
	'L_TINYEDITOR_popupWarning'		=> 'Impossible de dobrir una fenestra popup',
	'L_TINYEDITOR_insertMedia'		=> 'Inserir lo mèdia',
	'L_TINYEDITOR_big_smile'		=> 'Grand sorire',
	'L_TINYEDITOR_mad'				=> 'Sès fadat !',
	'L_TINYEDITOR_bold'				=> 'gras',
	'L_TINYEDITOR_italic'			=> 'italic',
	'L_TINYEDITOR_underline'		=> 'soslinhat',
	'L_TINYEDITOR_strikethrough'	=> 'raiat',
	'L_TINYEDITOR_forecolor'		=> 'Color del tèxte',
	'L_TINYEDITOR_backcolor'		=> 'color del fons',
	'L_TINYEDITOR_link'				=> 'Ligam Internet',
	'L_TINYEDITOR_unlink'			=> 'suprimir lo ligam internet',
	'L_TINYEDITOR_removeformat'		=> 'suprimir tot format',
	'L_TINYEDITOR_justifyleft'		=> 'alinhar a man esquèrra',
	'L_TINYEDITOR_justifycenter'	=> 'centrar',
	'L_TINYEDITOR_justifyright'		=> 'alinhar a man drecha',
	'L_TINYEDITOR_insertorderedlist'=>'lista numerotada',
	'L_TINYEDITOR_insertunorderedlist'=>'lista ordinària',
	'L_TINYEDITOR_subscript'		=> 'metre en indici',
	'L_TINYEDITOR_superscript'		=> 'metre en exponent',
	'L_TINYEDITOR_html'				=> 'veire còdi sorga',
	'L_TINYEDITOR_fullscreen'		=> 'mòde plena ecran',
	'L_TINYEDITOR_musttextmode'		=> 'Passar en mòde tèxte per salvagardar',
	'L_TINYEDITOR_disabledCmd'		=> 'Aquesta comanda es desactivada en mòde HTML'
);
?>