<?php if(!defined('PLX_ROOT')) exit; ?>



<p>Avant de télécharger un plugin, veuillez vérifier sur <a href="https://forum.pluxml.org/viewforum.php?id=21" target="_blank">les forums PluXml</a> si une version plus récente n'est pas disponible dans la rubrique dédiée. </p>

<p><img alt="" src="https://ressources.pluxopolis.net/data/medias/images-site/attention-mec.tb.jpg" style="margin-left: 5px; margin-right: 5px; float: left; width: 30px; height: 30px;" /><strong>Note :</strong> Certains, il commence à y en avoir beaucoup, sont <strong>relativement anciens</strong> et ne fonctionnent plus avec les versions modernes de PluXml.<br />
Si vous ne les connaissez pas, il est fortement recommandé de les évaluer sur un PluXml&nbsp;de test <strong>avant de les installer en production</strong>.</p>



<p><font color=#FF0000><strong>Merci de nous signaler tous dysfonctionnements </font></strong> afin que nous retirions les présumés coupables de la liste jusqu'à une éventuelle mise à jour.</p>

<p>Vous pouvez bien sûr nous proposer vos plugins favoris pour autant qu'ils soient compatibles avec les versions 5.6 et 5.7 PluXml</p>


