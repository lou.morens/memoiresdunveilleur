<p>Bienvenue dans mon univers qui est consacré en grande partie, pour ne pas dire entièrement, aux Mémoires d'un
    Veilleur. </p>
<p>Dans cet univers de science-fiction, on retrouve de la fantasy traditionnelle et des références aux mythologies
    terriennes. Le premier tome,
    L'Année perdue, est paru aux éditions Atramenta en juillet 2018 (2ème édition). L'histoire se déroule sur la Terre
    et présente les personnages principaux. Les tomes suivants se déroulent en partie sur la Terre et sur les autres
    planètes décrites dans cet univers. Quelques nouvelles se déroulent sur la Terre avant l'Année perdue. Elles
    permettent de faire plus ample connaissance avec chaque personnage. Vous pouvez d'ores et déjà découvrir Coopération
    et vérité et Rencontres. Les tomes et les nouvelles sont aussi disponibles en lecture libre sur le site de
    l’éditeur. Plusieurs autres nouvelles, dont certaines traduites en anglais, sont aussi disponibles en lecture libre.
</p>
<p><br /></p>
<div align="center"><img src="data/medias/illustrations/insignes/insigne-gen.tb.jpg" title="insigne-gen.tb.jpg"
        alt="insigne-gen.jpg" /></div>
<p><br /></p>
<p>Cette image représente les préceptes que tout Veilleur doit respecter, <strong>R</strong>espect,
    <strong>P</strong>rotection, <strong>J</strong>ustice et <strong>S</strong>auvegarde. Toutes les informations
    concernant cet univers sont regroupées dans le wiki. Vous trouverez des informations non seulement sur l'Ordre des
    Veilleurs, mais aussi sur les personnages, les planètes, les peuples, etc. </p>
<p><br /></p>
<p>Les mémoires d'un Veilleur regroupent les aventures du Veilleur Jack Cooper et de ses compagnons, Vlad, Joshua,
    Alexandre, Solène, Baptiste, Charlie, etc, ainsi que celles de leurs descendants. Je mettrai les généalogies et les
    chronologies à jour au fur et à mesure de la mise à disposition des romans/nouvelles.</p> <br />
<div class="separation"></div>
<br />
<p><strong><br /></strong></p>
<p><strong>Le Blog</strong> regroupe des articles qui décrivent les personnages, l'univers, les outils utilisés pour
    écrire, les bilans... Il n'existe que depuis début 2018. J'ajouterai des articles régulièrement, n'hésitez pas à le
    consulter et à laisser vos commentaires. </p> <br />
<div class="separation"></div>
<br />
<h3><br /></h3><strong>Les retours de lectures </strong>
<p>L'Année perdue : Les chroniques de Lee Ham</p>
<p><a href="https://litteratutemltipleunerichesse.wordpress.com/2019/01/13/lannee-perdue-memoires-dun-veilleur-louise-morens-2018?fbclid=IwAR0YRzsqtjPpheYDYd6H0W2lGdCKE2cFo73KYHSs9Y2A5PQASdlumhcSARk"
        target="_blank">https://litteratutemltipleunerichesse.wordpress.com/2019/01/13/lannee-perdue-memoires-dun-veilleur-louise-morens-2018</a>
</p>
<h3> </h3> <br />
<div class="separation"></div>
<br />
<h3><br /></h3><strong>Les interviews </strong>
<p style="margin-bottom: 0cm"><a href="https://mambiehlblog.wordpress.com/2019/01/24/flash-auteur-15-louise-morens/"
        target="_blank">Les chroniques pressées</a> : <a
        href="https://chroniquespressees.com/2019/01/24/flash-auteur-15-louise-morens/" target="_blank">Flash auteur</a>
</p>
<p>Interview par <a href="http://lecturesfamiliales.wordpress.com/" target="_blank">Les lectures familiales</a> : <a
        href="https://lecturesfamiliales.wordpress.com/2019/03/12/jeu-auteur-mystere-27/" target="_blank">jeu de
        l'auteur mystère</a> et la <a
        href="https://lecturesfamiliales.wordpress.com/2019/03/16/jeu-auteur-mystere-27bis/?fbclid=IwAR0ZoaSKkBOcAsxuJh611VkkbzGZYnwXOS0jNPYKIH-DvHU05A9f6DZGa8g"
        target="_blank">réponse au jeu</a> </p>