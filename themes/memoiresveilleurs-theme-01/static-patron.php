<?php include(dirname(__FILE__).'/header.php'); ?>

<main class="main">

    <div class="container">

        <div class="grid">
          
            <div class="wiki-page col sml-12 med-9">

                <article class="article static" id="static-page-<?php echo $plxShow->staticId(); ?>">

                    <header>
                        <h2 class="wiki-title-page">
                        <?php $plxShow->lang('THANKS') ?>
                        </h2>
                    </header>
                    <div class="wiki-content">
                        <?php         
							include(dirname(__FILE__).'/lib-patron.php');
						?>
                    </div>
                </article>

            </div>

            <?php include(dirname(__FILE__).'/sidebar.php'); ?>

        </div>

    </div>

</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>