<?php if(!defined('PLX_ROOT')) exit; ?>

<li class="light-gray-background-color-border ">
    <a
        href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static12/odv"><?php $plxShow->lang('ORDERWATCHMEN') ?></a>
</li>
<li class="light-gray-background-color-border ">
    <a
        href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static13/odva"><?php $plxShow->lang('ORDERVAL') ?></a>
</li>
<li class="light-gray-background-color-border ">
    <a href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static5/planets"><?php $plxShow->lang('PLANET') ?></a>
</li>
<li class="light-gray-background-color-border ">
    <a
        href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static6/characters"><?php $plxShow->lang('CHARACTERS') ?></a>
</li>
<li class="light-gray-background-color-border ">
    <a
        href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static11/charactersinterview"><?php $plxShow->lang('CHARACTERSINTERVIEW') ?></a>
</li>
<li class="light-gray-background-color-border ">
    <a href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static7/etymology"><?php $plxShow->lang('ETYMOLOGY') ?></a>
</li>
<li class="light-gray-background-color-border ">
    <a href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static8/genealogy"><?php $plxShow->lang('GENEALOGY') ?></a>
</li>
<li class="light-gray-background-color-border ">
    <a href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static9/period"><?php $plxShow->lang('PERIOD') ?></a>
</li>
<li class="light-gray-background-color-border ">
    <a
        href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static10/illustrations"><?php $plxShow->lang('ARTWORKS') ?></a>
</li>
<li class="light-gray-background-color-border ">
    <a
        href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static16/music"><?php $plxShow->lang('SOUND_THEME') ?></a>
</li>