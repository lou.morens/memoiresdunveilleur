<?php include(dirname(__FILE__).'/header.php'); ?>

	<main class="main">

		<div class="container">

			<div class="grid">

				<div class="content col sml-12 med-9">

					<article class="article static" id="static-page-<?php echo $plxShow->staticId(); ?>">

                    <header>
                        <h2 class="wiki-title-page">
                            <?php $plxShow->staticTitle(); ?>
                        </h2>
                    </header>
                    <div class="wiki-content">
                        <?php                             
								include(dirname(__FILE__).'/lib-gamevn.php');
							?>
                    </div>
                </article>

            </div>
            <?php include(dirname(__FILE__).'/sidebar.php'); ?>


        </div>

    </div>

</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>