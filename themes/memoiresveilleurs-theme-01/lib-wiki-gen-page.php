<?php if (!defined('PLX_ROOT')) exit; 

// depends of:
// $folder: the folder with the collection of json to display
// Start scanning $folder/*.json
$folder = "wiki_pages/general/";
$jsonpath = $folder.$pageToDisplay.".json";

$contents = file_get_contents($jsonpath);
$get = json_decode($contents); 
echo '<div class="wiki-item" id="gen-'.$pageToDisplay.'">';

$name = $get->{'name'}->{$lang};
$dataFile = $get->{'content'}->{$lang}; 
$pageDisplay = $folder."content/".$dataFile;

echo '<h3 class="wiki-title-item">'.$name.'</h3>';  
$data = file_get_contents($pageDisplay);
echo '<div>';
$txt = html_entity_decode($data);
echo $txt;
echo '</div>';
//echo '<br/><div class="separation"></div><br/>';
    
?>