<?php if(!defined('PLX_ROOT')) exit; 

$jsonpath = "wiki_pages/patrons/patrons.json";
$contents = file_get_contents($jsonpath);
$get = json_decode($contents); 
$patron = $get->{'patron'};


echo '<div class="col-1">';
echo '<div><h3><img src="data/medias/illustrations/insignes/insigne-itinerant.tb.png" alt="itinerant watchman badge"/></h3><ul>';
    foreach($patron as $npatron) {
    
       if($npatron->{'type'} === "simple"){
        echo '<li>';
        if($npatron->{'url'}){
            echo ' <a href="'.$npatron->{'url'}.'" target="_blank" rel="noopener, noreferrer"><b>'.$npatron->{'name'}.'</b></a>';
        } else {
            echo '<b>'.$npatron->{'name'}.'</b>';
        }  
        echo'</li>';
       }
     
    }
    echo '</ul></div>';
/*prevision pour tipeee*/
    echo '<div><h3><img src="data/medias/illustrations/insignes/insigne-attache.tb.png" alt="attached watchman badge"/></h3>';
    foreach($patron as $npatron) {
       if($npatron->{'type'} === "patreon3"){
         echo $npatron->{'name'}.', ';
        }       
    }
    echo '</div>';

    echo '<div><h3><img src="data/medias/illustrations/insignes/insigne-historien.tb.png" alt="historian watchman badge"/></h3><ul>';
    foreach($patron as $npatron) {
       if($npatron->{'type'} === "patreon1"){
         echo '<li>';
         echo $npatron->{'name'};
         echo '</li>';
        }       
    }
    echo '</ul>';
    echo '</div>';
    echo '</div>';