<?php if (!defined('PLX_ROOT')) exit; 

// depends of:
// $folder: the folder with the collection of json to display
// Start scanning $folder/*.json
$folder = "wiki_pages/gamevn/";
$jsonpath = $folder."vn-001.json";
$contents = file_get_contents($jsonpath);

$getall = json_decode($contents); 

$get = $getall->{'vnovel'};
foreach($get as $data){    
    echo '<div class="wiki-item" id="game-'.$data->{'id'}.'">';
    echo '<h3 class="wiki-title-item">'.$data->{'title'}->{$lang}.'</h3>'; 
    $desc = $data->{'desc'}->{$lang};
    if ($data->{'img'} !== "" ){  
        echo '<figure class="wiki-figure">';
        $alt = $data->{'name'}->{$lang};
        $descimg = $data->{'desc'}->{$lang};
        
        echo '<img src="'.$data->{'img'}->{'uri'}.'" class="wiki-img" alt="'.$alt.' desc="'.$descimg.'"/>';
        
        echo '<figcaption class="wiki-caption">'.$desc.'</figcaption>';
        echo '</figure>';
    }
    $date = $data->{'download'}->{'datepubli'}->{$lang};
    $urlpage = $data->{'download'}->{'urlpage'};
    $urlembed =  $data->{'download'}->{'urlembed'};
    $titleembed =  $data->{'download'}->{'title'};
   

    echo '<p>';
    $plxShow->lang('PUBLISH_DATE');
    echo ' : '.$date.'</p>';

    echo '<iframe src="'.$urlembed.'" width="552" height="167" frameborder="0"><a href="'.$urlpage.'">'.$titleembed.'</a></iframe>';
    echo '</div>';
}

      


?>