<?php include(dirname(__FILE__).'/header.php'); ?>

<main class="main">

    <div class="container">

        <div class="grid">
            <div class="menu-wiki-bg col sml-12 med-3">

                <ul class="no-style-liste menu-wiki-ul">
                    <?php include(dirname(__FILE__).'/menu-wiki.php'); ?>
                </ul>

            </div>
            <div class="wiki-page col sml-12 med-9">

                <article class="article static" id="static-page-<?php echo $plxShow->staticId(); ?>">

                    <header>
                        <h2>
                            <?php $plxShow->staticTitle(); ?>
                        </h2>
                    </header>

                    <?php $plxShow->staticContent(); ?>


                    <?php
                            $pageToDisplay = "2D";
                            include(dirname(__FILE__).'/lib-galery.php');
                        ?>
                    <br />
                    <div class="separation"></div>
                    <br />

                    <?php
                            $pageToDisplay = "3D";
                            include(dirname(__FILE__).'/lib-galery.php');
                        ?>
                    <br />
                    <div class="separation"></div>
                    <br />

                    <?php
                            $pageToDisplay = "insignes";
                            include(dirname(__FILE__).'/lib-galery.php');
                        ?>
                </article>

            </div>



        </div>

    </div>

</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>