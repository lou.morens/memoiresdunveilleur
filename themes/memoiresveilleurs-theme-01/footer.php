<?php if (!defined('PLX_ROOT')) exit; ?>

	<footer class="footer">
		<div class="container">
			<p>
				<?php $plxShow->mainTitle('link'); ?> - <?php $plxShow->subTitle(); ?> &copy; 2018
			</p>
		
			
			<p>
				<?php $plxShow->lang('FOLLOW_ME') ?>&nbsp; : 
				<a href="https://framapiaf.org/@louisemorens" title="Mastodon" target="_blank" rel="nofollow noreferrer"> Mastodon</a>&nbsp; - &nbsp; 
				<a href="https://www.facebook.com/louisemorensautrice" title="Facebook" target="_blank" rel="nofollow noreferrer">Facebook</a>&nbsp; - &nbsp;           
				<a href="https://twitter.com/LouiseMorens" title="Twitter" target="_blank" rel="nofollow noreferrer">Twitter</a>&nbsp; - &nbsp; 
				<a href="https://www.instagram.com/louisemorens" title="Instagram" target="_blank" rel="nofollow noreferrer">Instagram</a>&nbsp; - &nbsp; 
				<a href="https://www.youtube.com/channel/UCgS-jM7f8H7N1MOTzur5ZaQ" title="youtube" target="_blank" rel="nofollow noreferrer">youtube</a>&nbsp; - &nbsp;
				<a href="https://www.deviantart.com/louisemorens" title="DeviantArt" target="_blank" rel="nofollow noreferrer">DeviantArt</a>&nbsp; - &nbsp;
				<a href="https://www.atramenta.net/authors/louise-morens/77767" title="atramenta" target="_blank" rel="nofollow noreferrer">Atramenta</a>&nbsp; - &nbsp;
				<a href="https://fr.tipeee.com/memoires-dun-veilleur" title="Tipeee" target="_blank" rel="nofollow noreferrer">Tipeee</a>
            </p>
			<p>
				<a href="https://copyrightdepot.com/showCopyright.php?lang=FR&id=6777" title="copyrightdepot" target="_blank" rel="nofollow noreferrer">copyrightdepot n° 00061238-8</a>
			</p>
			<ul class="menu">
				<li><a href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" title="<?php $plxShow->lang('ARTICLES_RSS_FEEDS'); ?>"><?php $plxShow->lang('ARTICLES'); ?></a></li>
				<li><a href="<?php $plxShow->urlRewrite('#top') ?>" title="<?php $plxShow->lang('GOTO_TOP') ?>"><?php $plxShow->lang('TOP') ?></a></li>
			</ul>
			<p><?php $plxShow->lang('TRANSLATE') ?> - 
			<a href="<?php $plxShow->racine() ?><?php echo $lang; ?>/static4/termsofuses"><?php $plxShow->lang('TERMOFUSE') ?></a></p>
			<p>
				<?php $plxShow->lang('POWERED_BY') ?>&nbsp;<a href="http://www.pluxml.org" title="<?php $plxShow->lang('PLUXML_DESCRIPTION') ?>">PluXml</a>
				<?php $plxShow->lang('IN') ?>&nbsp;<?php $plxShow->chrono(); ?>&nbsp;			
			</p>
		</div>
	</footer>

</body>

</html>
