<?php

$LANG = array(

#header.php
'MENU'					=> 'Menu',
'HOME'					=> 'Accueil',
'GOTO_CONTENT'			=> 'Aller au contenu',
'GOTO_MENU'				=> 'Aller au menu',
'COMMENTS_RSS_FEEDS'	=> 'Fil Rss des commentaires',
'COMMENTS'				=> 'Fil des commentaires',
'ARTICLES_RSS_FEEDS'	=> 'Fil Rss des articles',
'ARTICLES'				=> 'Fil des articles',

# sidebar.php
'CATEGORIES' 			=> 'Catégories',
'LATEST_ARTICLES'		=> 'Derniers articles',
'LATEST_COMMENTS'		=> 'Derniers commentaires',
'ARCHIVES'				=> 'Archives',

# footer.php
'POWERED_BY'			=> 'Généré par',
'PLUXML_DESCRIPTION'	=> 'Blog ou Cms sans base de données',
'IN'					=> 'en',
'ADMINISTRATION'		=> 'Administration',
'GOTO_TOP'				=> 'Remonter en haut de page',
'TOP'					=> 'Haut de page',
'TRANSLATE'             => 'Traduit par Louise Morens',
'TERMOFUSE'             => 'Mentions légales',


# erreur.php
'ERROR'					=> 'La page que vous avez demandée n\'existe pas',
'BACKTO_HOME'			=> 'Retour à la page d\'accueil',

# common
'WRITTEN_BY'			=> 'Rédigé par',
'CLASSIFIED_IN'			=> 'Classé dans',
'TAGS'					=> 'Mots clés',

# commentaires.php
'SAID'					=> 'a dit',
'WRITE_A_COMMENT'		=> 'Écrire un commentaire',
'NAME'					=> 'Votre nom ou pseudo',
'WEBSITE'				=> 'Votre site Internet (facultatif)',
'EMAIL'					=> 'Votre adresse e-mail (facultatif)',
'COMMENT'				=> 'Contenu de votre message',
'CLEAR'					=> 'Effacer',
'SEND'					=> 'Envoyer votre commentaire',
'COMMENTS_CLOSED'		=> 'Les commentaires sont fermés',
'ANTISPAM_WARNING'		=> 'Vérification anti-spam',

'REPLY'					=> 'Répondre',
'REPLY_TO'				=> 'Répondre à',
'CANCEL'				=> 'Annuler',

#AJOUT LM
'FOLLOW_ME'				=> 'Me suivre',
'LANG_EN'				=> 'anglais',
'LANG_FR'				=> 'français',
'NOVEL'					=> 'Romans',
'NOVEL_PUB'				=> 'Publications',
'NOVEL_LIB'				=> 'Lecture libre',
'ABOUT'					=> 'À propos',
'IN_PROGRESS'           => 'Projet en cours',
'ILLUS_LIB'             => 'Illustrations',
'THANKS'                => 'Remerciements',
'OTHER'                 => 'Divers',
'MUSIC_LIB'             => 'Univers musical',
'RELATIVE_NOVEL'        => 'Roman ou nouvelle associée : ',
'VIDEO_MUSIC_LIB'       => 'Les videos associées aux thèmes musicaux.',
'TIPEEE'                => 'Soutenez Mémoires d\'un Veilleur sur Tipeee',
#Wiki menu
'PLANET'            => 'Les planètes',
'ORDERWATCHMEN'     => 'L\'ordre des Veilleurs',
'ORDERVAL'          => 'L\'Ordre de Valaquenta',
'CHARACTERS'        => 'Les personnages',
'CHARACTERSINTERVIEW'=>'Interviews des personnages',
'ETYMOLOGY'         => 'Ethymologie',
'GENEALOGY'         => 'Généalogie',
'PERIOD'            => 'Les périodes du cycle',
'ARTWORKS'          => 'Illustrations',
#Wiki data
'WIKI_DESC'         => 'Les mémoires d\'un Veilleurs regroupent plusieurs tomes qui narrent l\'histoire du Veilleur Jack Cooper, de ses compagnons et de leurs descendants plus ou moins proches.',
'PEOPLE'            => 'Peuple',
'PLANET_BIRTH'      => 'Planète d\'origine',
'DATE_BIRTH'        => 'Date de naissance',
'DATE_DEATH'        => 'Date de décès',
'AGEDIED'           => 'Âge lors du décès',
'DETAILED_PAGE'     => 'Voir la fiche détaillée',
'FREEREAD_PAGE'     => 'Lecture en ligne gratuite',
'SOUND_THEME'       => 'Thème musical associé',
'SOUND_THEME_CHAR'  => 'Thème musical associé aux personnages : ',
'SOUND_THEME_PLANET' => 'Thème musical associé à la planète : ',
#publi
'RESUME'            => 'Résumé',
'EXTRACT'           => 'Extrait',
'COVER'             => '4e de couverture',
'PAPER'             => 'Disponible au format papier sur le site de l\'éditeur',
'EBOOK'             => 'Disponible au format ebook sur le site de l\'éditeur',
'FREEREAD'          => 'Disponible en lecture libre',
'EDITOR'            => 'Éditeur',
'PUBLISH'           => 'publication ',
'SWITCHLANG'        => 'Lire la version anglaise',
#interviews
'INTERVIEWTITLE'    => 'Lire les interviews fictives des personnages',
'IDATE'             => 'Date',
'ISCOPE'            => 'Cadre',
'IPLACE'            => 'Lieu',
'IREPORTER'         => 'Reporteur',
#Progress
'NOVEL-PROGRESS'    => 'Écriture',
'VN-PROGRESS'       => 'Nouvelle visuelle/jeu',
#Visual Novel
'TITLE_PAGE'        => 'Nouvelles Visuelles',
'PUBLISH_DATE'      => 'Date de publication',
);

?>
